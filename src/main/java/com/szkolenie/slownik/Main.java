package com.szkolenie.slownik;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Podaj angielskie slowko");
        String word = sc.nextLine();

        Map<String, String> dictionary = new HashMap<>();
        dictionary.put("phone", "telefon");
        dictionary.put("home", "dom");
        dictionary.put("cat", "kot");
        dictionary.put("dog", "pies");

        // Wyszukiwanie w mapie po kluczu
        String translation = dictionary.get(word.toLowerCase());
        if (translation == null) {
            // Wyszukiwanie po wartosciach - rzadko spotykane
            for (Map.Entry<String, String> entry : dictionary.entrySet()) {
                if (entry.getValue().equals(word)) {
                    translation = entry.getKey();
                    break;
                }
            }
        }
        System.out.println(translation != null ? translation : "Brak slowa");
        System.out.println(dictionary);
    }
}
